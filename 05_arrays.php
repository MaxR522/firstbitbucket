<?php

// Create array

$arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];
$arr2 = [100, 200, 300, 400];
$numbers = [4,2,3,1,5];
$dup = [2, 5, 2, 7, 8, 8, 9];

// To print array, we use print_r
// print_r($arr);

// add or modify element at index
$arr[10] = 10;
$arr[0] = 555;

// Useful array method
is_array($arr); //return true or false

in_array(555, $arr); // check if the value is in the array

array_unique($dup); // remove duplicate value

array_search(1, $numbers); // search the value inside an array and return the index

count($arr) . "<br>"; // get length of the array

reset($arr). "<br>"; // get the first element

end($arr) . "<br>"; // get last element

// Stack and queue functions

array_push($arr, 15); // push an element into the end

array_pop($arr); // drop the last element

array_unshift($arr, 0); // insert element into the beginning

array_shift($arr); // drop the first element

array_merge($arr, $arr2); // concatenate 2 arrays

sort($numbers); // sort the array

array_reverse($arr); // reverse the array

array_slice($arr, 2); // to slice or discrad items of the array (2 first element)

array_slice($arr, 2, 2); // to take a slice of a specific length starting to the specified index

array_splice($arr, 2, 2); // remove the spliced elements of the array

// array_map("", $arr); //iterate each element of the array and return new modified array

max($arr); // return the max value of the array
min($arr); // return the minimum value of the array
array_rand($arr); //return random value of the array

array_sum($arr); // add all values of the array

// array_reduce($arr, ''); //same as reduce in js and ruby

// https://www.php.net/manual/en/ref.array.php

// ============================================
// Associative arrays
// ============================================

// Create an associative array
// Associative arrays are arrays that use named keys that you assign to them.
$age = array("peter" => 12, "mario" => 24, "ranja" => 22);

$age["Aria"] = 12;
$age["Njara"] = 78;
$age["Hahaha"] = 0;



array_keys($age); // get an array of keys
array_values($age); // get an array of values

array_key_exists("Aria", $age); // to check if the key exists

// Multidimensional array

$multiArray = [ 
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9],
];



?>