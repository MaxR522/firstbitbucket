<?php

// Function which prints "Hello I am Zura"
function hello($name){
  echo "Hello $name !";
}

// Function with argument

function sum($a, $b){
  return $a + $b;
} 

// Create function to sum all numbers using ...$nums
// Using spread operator (...) catch all arguments passed in the function inform of array
function infinite_sum(...$sums){
  $res = 0;
  foreach($sums as $num){
    $res += $num;
  }
  return $res;
}

echo infinite_sum(1, 5, 7, 8);

// Arrow functions

// DON"T LIKE IT, SORRY
