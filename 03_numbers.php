<?php

// - Integers
  $int = 22;
  is_int($int);
// - Float 
  $float = 1.2;
  is_float($float);
// - Infinity (A numeric value that is larger than PHP_FLOAT_MAX)
  $inf = 1.9e411;
  is_infinite($inf);
// - NaN 
  $nan = acos(8);
  is_nan($nan);
// - Numerical Strings
  $numStr = "123";
  is_numeric($numStr);

  // Cast to int
  $intStr = (int)$numStr;
  echo gettype($intStr);
?>
