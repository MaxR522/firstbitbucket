<?php

$age = 20;
$salary = 300000;

// Sample if
if($age > 18){
  echo "too young";
}else{
  echo "allowed";
}

// Without circle braces

if ($salary < 200000){
  echo "more please";
} elseif($salary === 300000){
  echo "Good";
} else {
  echo "like a boss";
}

// Sample if-else

// Difference between == and ===

// if AND

// if OR

// Ternary if

$name = ($age > 40) ? "Robert" : "";

// Short ternary

// Null coalescing operator

// switch
$note = 10;

switch ($note)
{ 
    case 0: // dans le cas où $note vaut 0
        echo "Tu es vraiment un gros nul !!!";
    break;
    
    case 5: // dans le cas où $note vaut 5
        echo "Tu es très mauvais";
    break;
    
    case 7: // dans le cas où $note vaut 7
        echo "Tu es mauvais";
    break;
    
    case 10: // etc. etc.
        echo "Tu as pile poil la moyenne, c'est un peu juste…";
    break;
    
    case 12:
        echo "Tu es assez bon";
    break;
    
    case 16:
        echo "Tu te débrouilles très bien !";
    break;
    
    case 20:
        echo "Excellent travail, c'est parfait !";
    break;
    
    default:
        echo "Désolé, je n'ai pas de message à afficher pour cette note";
}
