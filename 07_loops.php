<?php

$numbers = [4,2,3,1,5];
$age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");

// while

$counter = 0;

while ($counter < count($numbers)){
   $numbers[$counter] . "<br>";
  $counter++;
}

// continue statement: skip this step
// break statement: stop the loop

// Loop with $counter


// do - while

$index = 0;
do {
  if($numbers[$index] < 3){
     $numbers[$index] . "<br>";
  }
  $index++;
}while($index < count($numbers));

// for

for ($i = 0; $i < count($numbers); $i++){
  if($numbers[$i] % 2 == 0){
     $numbers[$i] . "<br>";
  }
}

// foreach

foreach($numbers as $number) {
  if($number % 2 !== 0){
     $number . "<br>";
  }
}

// Iterate Over associative array.

foreach($age as $key => $value){
  echo "$key => $value <br>";
}

?>