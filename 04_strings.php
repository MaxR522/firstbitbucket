<?php

// Create simple string
$name = "Jack";
$age = 20;

// String concatenation
$phrase =  "Hello $name, you are $age years old";
$dot =  "hello" . $name . ", you are" . $age . "years old"; // with dot

// String functions
strlen($phrase); // get the length of the string
substr($name, 2, 1); // get substring starting to an index specified
$strArr = explode(" ",$name); // split into array (separator, string);
implode(" ", $strArr); // join the array into string


// https://www.php.net/manual/en/ref.strings.php

?>
